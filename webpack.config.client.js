var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');
const DEV = process.env.NODE_ENV !== 'production';

module.exports = {
    bail: !DEV,
    devtool: DEV ? 'cheap-module-source-map' : 'source-map',
  entry: {
    app: './src/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist/client'),
    filename: 'client.bundle.js'
  },
  module: {
    rules:[
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
        {
            test: /\.css/,
            loader: 'style-loader!css-loader',
        },
        {
            test: /\.(png|jpe?g|gif|svg)$/,
            loader: 'file-loader',
        },
        {
            test: /\.(graphql|gql)$/,
            exclude: /node_modules/,
            loader: 'graphql-tag/loader',
        }

    ]
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    hot: true,
    port: 4200,
    stats: "errors-only",
    open: true
  },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(DEV ? 'development' : 'production'),
            },
        }),
        DEV && new webpack.optimize.UglifyJsPlugin({
            compress: {
                screw_ie8: true, // React doesn't support IE8
                warnings: false,
            },
            mangle: {
                screw_ie8: true,
            },
            output: {
                comments: true,
                screw_ie8: true,
            },
        }),
        DEV && new webpack.optimize.AggressiveMergingPlugin(),
    ].filter(Boolean)
}
